package coba.actionkey;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import org.openqa.selenium.JavascriptExecutor;

import utility.Log;

public class TestForgotPassword {
	public static WebDriver driver;
	
	public static void main(String[] args) throws Exception {
		File file = new File("./driver/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver=new ChromeDriver();
		Log.info("Chrome browser started");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
//		driver.get("http://toolsqa.wpengine.com/automation-practice-table");
		driver.get("http://automationpractice.com/index.php");
		
		driver.findElement(By.xpath("//a[@class='login']")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("login_form")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("email")).sendKeys("rimarahmadani@mail.com");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='login_form']/div/p[1]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("email")).sendKeys("rima@mail.com");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='form_forgotpassword']/fieldset/p/button")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='center_column']/div/p")).click();
		Thread.sleep(1000);
		
		driver.close();
	}
}