package coba.actionkey;

import java.io.File;
//import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

//import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

//import org.openqa.selenium.JavascriptExecutor;

import utility.Log;

public class TestCreateAnAccount {
	public static WebDriver driver;
	
	public static void main(String[] args) throws Exception {
		File file = new File("./driver/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver=new ChromeDriver();
		Log.info("Chrome browser started");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
//		driver.get("http://toolsqa.wpengine.com/automation-practice-table");
		driver.get("http://automationpractice.com/index.php");
		
		driver.findElement(By.xpath("//a[@class='login']")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("email_create")).sendKeys("rimarahmadani@mail.com");
		Thread.sleep(1000);
		driver.findElement(By.id("SubmitCreate")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("id_gender2")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("customer_firstname")).sendKeys("Rima");
		Thread.sleep(1000);
		driver.findElement(By.id("customer_lastname")).sendKeys("Rahmadani");
		Thread.sleep(1000);
		driver.findElement(By.id("passwd")).sendKeys("12345");
		Thread.sleep(1000);
		driver.findElement(By.id("days")).sendKeys("12");
		Thread.sleep(1000);
		driver.findElement(By.id("months")).sendKeys("March");
		Thread.sleep(1000);
		driver.findElement(By.id("years")).sendKeys("1997");
		Thread.sleep(1000);
		driver.findElement(By.id("firstname")).sendKeys("Rima");
		Thread.sleep(1000);
		driver.findElement(By.id("lastname")).sendKeys("Rima");
		Thread.sleep(1000);
		driver.findElement(By.id("company")).sendKeys("Sale Stock");
		Thread.sleep(1000);
		driver.findElement(By.id("address1")).sendKeys("Tangerang");
		Thread.sleep(1000);
		driver.findElement(By.id("address2")).sendKeys("Tangerang");
		Thread.sleep(1000);
		driver.findElement(By.id("city")).sendKeys("Tangerang");
		Thread.sleep(1000);
		driver.findElement(By.id("id_state")).sendKeys("Indiana");
		Thread.sleep(1000);
		driver.findElement(By.id("postcode")).sendKeys("32343");
		Thread.sleep(1000);
		driver.findElement(By.id("phone_mobile")).sendKeys("242351");
		Thread.sleep(1000);
		driver.findElement(By.id("alias")).sendKeys("Office");
		Thread.sleep(1000);
		driver.findElement(By.id("submitAccount")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='center_column']/h1"));
		Thread.sleep(1000);
		driver.close();
	}
}