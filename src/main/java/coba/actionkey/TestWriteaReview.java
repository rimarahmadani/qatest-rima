package coba.actionkey;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import org.openqa.selenium.JavascriptExecutor;

import utility.Log;

public class TestWriteaReview {
	public static WebDriver driver;
	
	public static void main(String[] args) throws Exception {
		File file = new File("./driver/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		driver=new ChromeDriver();
		Log.info("Chrome browser started");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
//		driver.get("http://toolsqa.wpengine.com/automation-practice-table");
		driver.get("http://automationpractice.com/index.php");
		
		driver.findElement(By.xpath("//a[@class='login']")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("email")).sendKeys("rima@mail.com");
		Thread.sleep(1000);
		driver.findElement(By.id("passwd")).sendKeys("12345");
		Thread.sleep(1000);
		driver.findElement(By.id("SubmitLogin")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='block_top_menu']/ul/li[2]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='categories_block_left']/div/ul/li[1]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='list']/a/i")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='center_column']/ul/li[1]/div/div/div[3]/div/div[2]/a[2]/span")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='product_comments_block_extra']/ul/li/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='criterions_list']/li/div[1]/div[6]/a")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("comment_title")).sendKeys("Test Title");
		Thread.sleep(1000);
		driver.findElement(By.id("content")).sendKeys("test content");
		Thread.sleep(1000);
		driver.findElement(By.id("submitNewMessage")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='product']/div[2]/div/div/div"));
		Thread.sleep(1000);
		
		driver.close();
	}
}